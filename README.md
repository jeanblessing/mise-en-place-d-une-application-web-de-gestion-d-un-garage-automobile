# Mise en place d une application web de gestion d un garage automobile

Création d'une application web de gestion d'un  garage automobile conçue avec Laravel et Livewire

Tableau de bord des engins

Tableau des pannes diagnostiquées

Tableau des pannes réparées

Tableau de bord mécanicien

Tableau de bord client

LES FONCTIONS
#Client

le      client s'inscrira et se connectera au système
le      client peut faire une demande de service de son véhicule en fournissant des détails (numéro de véhicule, modèle, description du problème, etc.)
Une      fois la demande approuvée par l'administrateur, le client peut vérifier le coût, l'état du service
le      client peut supprimer la demande (enquête) si le client change d'avis ou n'est pas approuvé par l'administrateur (UNE SEULE DEMANDE EN ATTENTE PEUT ÊTRE SUPPRIMÉE)

le      client peut vérifier l'état de la demande (enquête) en attente, approuvée,      réparée, réparée, publiée
le      client peut vérifier les détails de la facture ou les véhicules réparés
le      client peut envoyer des commentaires à l'administrateur
le client      peut voir/modifier son profil

#Mécanicien

le      mécanicien postulera à un emploi en fournissant des détails tels que      (compétences, adresse, mobile, etc.)
L'administrateur      embauchera (approuvera) le compte de mécanicien en fonction de ses      compétences
Après      l'approbation du compte, le mécanicien peut se connecter au système
le      mécanicien peut voir combien de travaux (véhicules à réparer) m'est confié
le      mécanicien peut changer l'état du service ('Réparation', 'Réparation      terminée') en fonction de l'avancement des travaux
le      mécanicien peut voir le salaire et le nombre de véhicules qu'il a réparés      jusqu'à présent
le      mécanicien peut envoyer des commentaires à l'administrateur
le      mécanicien peut voir/modifier son profil

#Administrateur

Le      premier administrateur se connectera (pour le nom d'utilisateur/mot de      passe)
Donnez      votre nom d'utilisateur, votre e-mail, votre mot de passe et votre compte      administrateur sera créé.
Après      la connexion, l'administrateur peut voir combien de clients, de      mécaniciens et de commandes de service récentes sur le tableau de bord
L'administrateur      peut voir/ajouter/mettre à jour/supprimer des clients
L'administrateur      peut voir chaque facture client (si deux demandes sont faites par le même      client, la somme totale des deux demandes sera affichée)
L'administrateur      peut voir/ajouter/mettre à jour/supprimer des mécanismes
L'administrateur      peut approuver (embaucher) des mécaniciens (requis par le mécanicien) en      fonction de leurs compétences
L'administrateur      peut voir/mettre à jour le salaire du mécanicien
L'administrateur      peut voir/mettre à jour/supprimer la demande/demande de service envoyée      par le client
L'administrateur      peut également faire une demande de service (supposons que le client      contacte directement le centre de service/bureau)
L'administrateur      peut approuver la demande de service faite par le client et assigner au      mécanicien pour la réparation et fournira le coût selon la description du      problème
L'administrateur      peut voir tous les coûts de service de la demande (approuvés et en      attente)
L'administrateur      peut voir les commentaires envoyés par le client/mécanicien

#Autres caractéristiques

nous      pouvons changer le thème du site Web jour (blanc) et nuit (noir)
si      le client est supprimé par l'administrateur, sa demande (enquête) sera      automatiquement supprimée
Autres points

	. l'application web sera déployée en local et en ligne (donc dans tes traveaux, il faut tenir compte )
	. je penses aussi que tu peux utiliser le template AdminLte ou d'autres qui font la même chose que çà dans le but de ranger les menus à gauche(le Side barre) sur le site 

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:ae3d1e6a31b32a14cb2e69eaeeb4fcae?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:ae3d1e6a31b32a14cb2e69eaeeb4fcae?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:ae3d1e6a31b32a14cb2e69eaeeb4fcae?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/jeanblessing/mise-en-place-d-une-application-web-de-gestion-d-un-garage-automobile.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:ae3d1e6a31b32a14cb2e69eaeeb4fcae?https://gitlab.com/jeanblessing/mise-en-place-d-une-application-web-de-gestion-d-un-garage-automobile/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:ae3d1e6a31b32a14cb2e69eaeeb4fcae?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:ae3d1e6a31b32a14cb2e69eaeeb4fcae?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:ae3d1e6a31b32a14cb2e69eaeeb4fcae?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:ae3d1e6a31b32a14cb2e69eaeeb4fcae?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:ae3d1e6a31b32a14cb2e69eaeeb4fcae?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:ae3d1e6a31b32a14cb2e69eaeeb4fcae?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:ae3d1e6a31b32a14cb2e69eaeeb4fcae?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:ae3d1e6a31b32a14cb2e69eaeeb4fcae?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:ae3d1e6a31b32a14cb2e69eaeeb4fcae?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:ae3d1e6a31b32a14cb2e69eaeeb4fcae?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:ae3d1e6a31b32a14cb2e69eaeeb4fcae?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

